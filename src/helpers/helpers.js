export function decWords (n, words) {
  return words[(n = (n = n % 100) > 19 ? (n % 10) : n) === 1 ? 0 : ((n > 1 && n <= 4) ? 1 : 2)]
}

export function sortCards (arr, states) {
  return arr.sort((i1, i2) => {
    return states.indexOf(i1.state) - states.indexOf(i2.state)
  })
}

export function getJsonData (fileName) {
  let jsonData = require(`../json/${fileName}`)
  return JSON.stringify(jsonData)
}

export function rand (min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min)
}
