import Vue from 'vue'
import Vuex from 'vuex'
import VuexI18n from 'vuex-i18n'

import modules from './modules'

Vue.use(Vuex)

const store = new Vuex.Store({
  strict: true, // process.env.NODE_ENV !== 'production',
  state: {},
  mutations: {},
  actions: {},
  modules
})

// Init modules
Object.keys(modules).forEach((modulName) => {
  if (modules[modulName].actions.init) {
    store.dispatch(`${modulName}/init`)
  }
})

Vue.use(VuexI18n.plugin, store)

Vue.i18n.add('ru', require('./i18n/ru.json'))
Vue.i18n.set('ru')
Vue.i18n.fallback('ru')

export default store
