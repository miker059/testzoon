import { getJsonData, rand } from '../../../helpers/helpers'

const jsonRawData = getJsonData('cards.json')

const updateRandomObject = ({ state, commit }) => {
  let jsonData = JSON.parse(jsonRawData)
  let obj = jsonData[Math.floor(Math.random() * jsonData.length)]

  Object.assign(obj, {
    state: state.states[Math.floor(Math.random() * state.states.length)],
    rating: rand(1, 5),
    review: rand(0, 200),
    reply: rand(0, 20),
    update: rand(0, 5)
  })

  commit('UPDATE_CARDS', obj)
}

const init = ({ commit, dispatch }) => {
  let jsonData = JSON.parse(jsonRawData)

  // Эмуляция загрузки с сервера
  let sendRequestEmulator = new Promise((resolve) => {
    setTimeout(() => {
      resolve(jsonData)
    }, 3000)
  })

  commit('SET', { t: 'isLoading', v: true })
  sendRequestEmulator.then((res) => {
    commit('SET', { t: 'isLoading', v: false })
    commit('SET_CARDS', res)

    setInterval(() => {
      dispatch('updateRandomObject')
    }, rand(10, 30) * 1000)
  })
}

export default {
  init,
  updateRandomObject
}
