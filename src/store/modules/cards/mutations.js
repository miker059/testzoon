import { sortCards } from '../../../helpers/helpers'

export default {
  SET (state, { t, v }) {
    state[t] = v
  },
  SET_CARDS (state, arrCards) {
    state.cards = sortCards(arrCards, state.states)
  },
  UPDATE_CARDS (state, obj) {
    let card = state.cards.find(item => item.id === obj.id)
    Object.assign(card, obj)
    state.cards = sortCards(state.cards, state.states)
  }
}
