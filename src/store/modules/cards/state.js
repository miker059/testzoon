const state = {
  cards: [],
  isLoading: false,
  states: ['purple', 'orange', 'grey']
}

export default state
