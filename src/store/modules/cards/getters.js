const getTotalReviews = (state) => {
  return state.cards.reduce((summ, item) => summ + item.review, 0)
}
const getTotalReplies = (state) => {
  return state.cards.reduce((summ, item) => summ + item.reply, 0)
}
const getTotalUpdates = (state) => {
  return state.cards.reduce((summ, item) => summ + item.update, 0)
}
const getTotalRating = (state) => {
  return state.cards.length ? (state.cards.reduce((summ, item) => summ + item.rating, 0) / state.cards.length).toFixed(1) : 0
}

export default {
  getTotalReviews,
  getTotalReplies,
  getTotalUpdates,
  getTotalRating
}
