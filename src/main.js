import Vue from 'vue'
import App from './App.vue'
import store from './store'
import VueNumber from 'vue-number-animation'

import SvgIcon from './components/common/SvgIcon.vue'
import ButtonComponent from './components/common/ButtonComponent.vue'

Vue.config.productionTip = false

Vue.component('svg-icon', SvgIcon)
Vue.component('btn-component', ButtonComponent)

Vue.use(VueNumber)

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
